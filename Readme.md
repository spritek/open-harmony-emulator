## OpenHarmonyEmulator

### License
``` shell
Copyright 2020 Singular Security Lab 

The following points clarify the project licenses:
The whole project is released under the Attribution-NoDerivs 4.0 Unported (CC BY-ND 4.0).
QEMU  is released under the GNU General Public License, version 2.
The Tiny Code Generator (TCG) is released under the BSD license.
```
### 简介
​    OpenHarmonyEmulator基于qemu项目，模拟了OpenHarmony Hi3518开发板，可以在虚拟环境中运行Open Harmony系统。
### 文件说明

本项目包含以下几个文件：
- qemu-system-arm qemu模拟器的主程序
- qemu-ifup和qemu-ifdown是qemu用来在宿主机上配置网卡的脚本文件
- liteos.bin 是OpenHarmony的debug版本的内核镜像
- rootfs.img 是OpenHarmony的debug版本的文件系统镜像
- flash 是模拟OpenHarmony的flash外设
- start_qemu.sh 一键运行脚本

### 环境依赖


Ubuntu 16.04
~~~ shell
sudo apt install libvte-dev qemu-utils ipxe-qemu libvdeplug-dev libfdt-dev vim git gettext g++  autoconf automake libtool  libaio1 libasound2 libbluetooth3 libbrlapi0.6 libc6 libcacard0 libfdt1 libgcc1 libglib2.0-0 libgnutls30 libjpeg8 libncurses5 libnettle6 libnuma1 libpixman-1-0 libpng12-0 libpulse0 libsasl2-2 libsdl1.2debian libseccomp2 libspice-server1  libstdc++6 libtinfo5 libusb-1.0-0 libusbredirparser1 libuuid1 libx11-6 libxen-4.6  libxenstore3.0 zlib1g qemu-system-common bridge-utils uml-utilities   
~~~

Ubuntu 20.04
~~~ shell
sudo apt install libvte-dev qemu-utils ipxe-qemu libvdeplug-dev libfdt-dev vim git gettext g++ autoconf automake libtool libaio1 libasound2 libbluetooth3 libc6 libcacard0 libfdt1 libgcc1 libglib2.0-0 libgnutls30 libjpeg8 libncurses5 libnuma1 libpixman-1-0 libpulse0 libsasl2-2 libsdl1.2debian libseccomp2 libspice-server1 libstdc++6 libtinfo5 libusb-1.0-0 libusbredirparser1 libuuid1 libx11-6 libxenstore3.0 zlib1g qemu-system-common libvdeplug2 bridge-utils uml-utilities
#libvdeplug.so.2:
sudo apt install libvdeplug2 
#libpng12.so.0:
wget https://nchc.dl.sourceforge.net/project/libpng/libpng12/1.2.59/libpng-1.2.59.tar.gz
tar zxvf libpng-1.2.59.tar.gz
cd libpng-1.2.59
./configure
make 
sudo make install
#libvte.so.9:
wget http://ftp.us.debian.org/debian/pool/main/v/vte2.91/libvte-2.91-0_0.38.1-2_amd64.deb
sudo dpkg -i libvte-2.91-0_0.38.1-2_amd64.deb
sudo apt install -f
~~~

### liteos.bin与rootfs.img构建
[Open Harmony  源码获取](https://gitee.com/OpenHarmony/docs/blob/master/get-code/%E6%BA%90%E7%A0%81%E8%8E%B7%E5%8F%96.md)

- release编译(默认)  python build.py ipcamera_hi3518ev300 ，没有shell
- Debug编译：python build.py ipcamera_hi3518ev300 -b debug

编译时需要禁用wifi编译选项,在config文件中将LOSCFG_DRIVERS_HDF_WIFI和LOSCFG_DRIVERS_HI3881设置为n。
Debug编译config：

- code-1.0/kernel/liteos_a/tools/build/config/debug/hi3518ev300_clang.config
- code-1.0/kernel/liteos_a/tools/build/config/debug/hi3518ev300.config

### 运行
OpenHarmonyEmulator提供了OpenHarmony的debug版本的liteos.bin和rootfs.img

- 无网卡启动:
   -  将liteos.bin、rootfs.img、flash和qemu-system-arm放在同一目录下
   -  sudo ./qemu-system-arm -M hi3518 -kernel liteos.bin -nographic

- 有网卡启动：
   - 将liteos.bin、rootfs.img、flash和qemu-system-arm放在同一目录下
   - 运行start_qemu.sh
   - 虚拟机ip是192.168.1.10，网桥ip是192.168.1.1，在宿主机中ping 192.168.1.10成功，说明设置网络成功
   
- 结束运行
   - 组合键ctrl +a，再按x，

![运行启动](./运行启动.PNG)

### 免责声明
OpenHarmonyEmulator由Singular Security Lab制作，并非Open Harmony官方工具。您可以自由使用本软件，但不得用于教育和研究之外的其他目的。由于使用本软件产生的一切法律及连带责任，与本软件和Singular Security Lab无关。

### 其他
如果您在使用该软件的过程中有任何问题，欢迎给我们留言，我们将尽力及时解答。